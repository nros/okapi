/*===========================================================================
  Copyright (C) 2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.tkit.writer;

import net.sf.okapi.common.StringParameters;

public class Parameters extends StringParameters {
	static final String REMOVETARGET = "removeTarget"; //$NON-NLS-1$
	static final String MESSAGE = "message"; //$NON-NLS-1$
	
	public Parameters () {
		super();
	}
	
	public void reset() {
		super.reset();
		setMessage("");
		setRemoveTarget(true);
	}

	public void fromString (String data) {
		super.fromString(data);
	}

	public String getMessage() {
		return getString(MESSAGE);
	}

	public void setMessage(String message) {
		setString(MESSAGE, message);
	}

	public boolean isRemoveTarget() {
		return getBoolean(REMOVETARGET);
	}

	public void setRemoveTarget(boolean removeTarget) {
		setBoolean(REMOVETARGET, removeTarget);
	}
}
