/*===========================================================================
  Copyright (C) 2008-2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.tkit.jarswitcher;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import net.sf.okapi.common.exceptions.OkapiIOException;

public class VersionInfo implements Comparable<VersionInfo> {

	private final static SimpleDateFormat LONG_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");
	private final static SimpleDateFormat SHORT_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
	
	private String label; // Unique label of this version
	private String path;  // Path to the version root jar
	private String date;  // date specifies the start date the jars of this version started being used for t-kit creation.

	public VersionInfo() {
		super();
	}
	
	/**
	 * 
	 * @param label
	 * @param path
	 * @param date the start date the jars of this version started being used for t-kit creation. 
	 * Expected date format is "yyyy-MM-dd" ("2014-04-08").
	 */
	public VersionInfo(String label, String path, String date) {
		super();
		this.label = label;
		this.path = path;
		this.date = date;
	}
	
	public String getLabel() {
		return label;
	}
	
	public void setLabel(String label) {
		this.label = label;
	}
	
	public String getPath() {
		return path;
	}
	
	public void setPath(String path) {
		this.path = path;
	}
	
	public String getDate() {
		return date;
	}
	
	/**
	 * 
	 * @param date the start date the jars of this version started being used for t-kit creation. 
	 * Expected date format is "yyyy-MM-dd" ("2014-04-08").
	 */
	public void setDate(String date) {
		this.date = date;
	}

	@Override
	public int compareTo(VersionInfo vi) {
		try {
			Date d = parseShortDate(date);		
			Date d2 = parseShortDate(vi.getDate());
			return d.compareTo(d2);
		} catch (ParseException e) {
			return 0;
		}		
	}

	public static Date parseLongDate(String date) throws ParseException {
		return LONG_DATE_FORMAT.parse(date);
	}
	
	public static Date parseShortDate(String date) throws ParseException {
		return SHORT_DATE_FORMAT.parse(date);
	}
	
	public static String getFileCreationDate(URL url) {
		try {
			return getFileCreationDate(new File(url.toURI()));
		} catch (URISyntaxException e) {
			return null;
		}
	}
	
	public static String getFileCreationDate(String path) {
		return getFileCreationDate(new File(path));
	}
	
	public static String getFileCreationDate(File file) {
		Path path = file.toPath();
		try {
			BasicFileAttributes attr = Files.readAttributes(path, BasicFileAttributes.class);		
			FileTime creationTime = attr.creationTime();
			long mils = creationTime.toMillis();
			
			Date d = new Date(mils);
			String date = SHORT_DATE_FORMAT.format(d);
			return date;
		} catch (IOException e) {
			throw new OkapiIOException(e);
		}		
	}

}
