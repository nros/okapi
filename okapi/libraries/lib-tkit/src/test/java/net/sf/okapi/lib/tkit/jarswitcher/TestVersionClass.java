/*===========================================================================
  Copyright (C) 2008-2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.tkit.jarswitcher;

import static org.junit.Assert.assertEquals;

import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.ProcessBuilder.Redirect;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URISyntaxException;

import net.sf.okapi.common.ClassUtil;

import org.junit.Test;

public class TestVersionClass {

	@Test
	/**
	 * VersionClass in resources v1/, v2/, v3/ is different, returns a corresponding version number. 
	 */
	public void testDynamicClassLoading() 
			throws ClassNotFoundException, InstantiationException, 
			IllegalAccessException, NoSuchMethodException, SecurityException, IllegalArgumentException, 
			InvocationTargetException {
		final VersionManager vm = new VersionManager();
//		vm.load(this.getClass().getResource("test.json"));
		
		vm.add("v1", this.getClass().getResource("/v1/").getPath(), "2014-04-10");
		vm.add("v2", this.getClass().getResource("/v2/").getPath(), "2014-04-11");
		vm.add("v3", this.getClass().getResource("/v3/").getPath(), "2014-04-12");
		
		assertEquals(3, vm.getVersions().size());
		
		vm.loadVersion("v1");
		ClassLoader cl = vm.getClassLoader();
		Class<?> cls = cl.loadClass("net.sf.okapi.lib.tkit.jarswitcher.VersionClass");
		Object obj = cls.newInstance();
		Method m = cls.getDeclaredMethod("getVersion", new Class[]{});
		Object ret = m.invoke(obj, new Object[]{});
		assertEquals("Version 1", ret);
		
		vm.loadVersion("v2");
		cl = vm.getClassLoader();
		cls = cl.loadClass("net.sf.okapi.lib.tkit.jarswitcher.VersionClass");
		obj = cls.newInstance();
		m = cls.getDeclaredMethod("getVersion", new Class[]{});
		ret = m.invoke(obj, new Object[]{});
		assertEquals("Version 2", ret);
		
		vm.loadVersion("v3");
		cl = vm.getClassLoader();
		cls = cl.loadClass("net.sf.okapi.lib.tkit.jarswitcher.VersionClass");
		obj = cls.newInstance();
		m = cls.getDeclaredMethod("getVersion", new Class[]{});
		ret = m.invoke(obj, new Object[]{});
		assertEquals("Version 3", ret);
		
		vm.loadVersion("v1");
		cl = vm.getClassLoader();
		cls = cl.loadClass("net.sf.okapi.lib.tkit.jarswitcher.VersionClass");
		obj = cls.newInstance();
		m = cls.getDeclaredMethod("getVersion", new Class[]{});
		ret = m.invoke(obj, new Object[]{});
		assertEquals("Version 1", ret);
		
		// Static link to the class remains
		VersionClass vc = new VersionClass();
		assertEquals("Version 0", vc.getVersion());
	}

	@Test
	public void testReflectionClassLoading()
			throws ClassNotFoundException, InstantiationException, 
			IllegalAccessException, NoSuchMethodException, SecurityException, IllegalArgumentException, 
			InvocationTargetException {
		VersionManager vm = new VersionManager();
		
		vm.add("v1", this.getClass().getResource("/v1/").getPath(), "2014-04-10");
		vm.add("v2", this.getClass().getResource("/v2/").getPath(), "2014-04-11");
		vm.add("v3", this.getClass().getResource("/v3/").getPath(), "2014-04-12");
		
		assertEquals(3, vm.getVersions().size());
		
		vm.loadVersion("v1");
		ClassLoader cl = vm.getClassLoader();
		Class<?> cls = cl.loadClass(this.getClass().getName());
		Object obj = cls.newInstance();
		Method m = cls.getDeclaredMethod("main", new Class[]{String[].class});
		Thread.currentThread().setContextClassLoader(cl);
		m.invoke(obj, new Object[]{new String[]{}});
	}
	
	@Test
	public void testStaticClassLoading() throws IOException, InterruptedException, URISyntaxException {
		final VersionManager vm = new VersionManager();
		
		vm.add("v1", this.getClass().getResource("/v1/").getPath(), "2014-04-10");
		vm.add("v2", this.getClass().getResource("/v2/").getPath(), "2014-04-11");
		vm.add("v3", this.getClass().getResource("/v3/").getPath(), "2014-04-12");
		
		assertEquals(3, vm.getVersions().size());
		
		ClassLoader sysClassLoader = ClassLoader.getSystemClassLoader();
		System.out.println("SysCL: " + sysClassLoader);
		ProcessBuilder pb =
				new ProcessBuilder(
						"java",
						"-cp",
						System.getProperty("java.class.path"), // classpath of this app
						"-Djava.system.class.loader=net.sf.okapi.lib.tkit.jarswitcher.VMClassLoader", 
						this.getClass().getName());
				
	   	pb.redirectOutput(Redirect.INHERIT);
	   	pb.redirectError(Redirect.INHERIT);
	   	Process p = pb.start();
	   	
	   	DataOutputStream dos = new DataOutputStream(p.getOutputStream());
	   	String appLibPath = this.getClass().getResource("/v2/").getPath();
	   	
	   	dos.writeUTF(appLibPath);
	   	dos.writeUTF(ClassUtil.getQualifiedClassName(this)); // appRootName
	   	dos.writeUTF(ClassUtil.getPath(this.getClass()));
	   	dos.writeUTF(ClassUtil.getClassFilePath(this.getClass()));
	   	dos.flush();
	   	
	   	System.out.println("Starting...\n");
	}
	
	public static void main(String[] args) {
		System.out.println("In main()");
		ClassLoader sysClassLoader = ClassLoader.getSystemClassLoader();
		System.out.println("SysCL: " + sysClassLoader);
		
		VersionClass vc = new VersionClass();
		System.out.println("VersionClass CL: " + vc.getClass().getClassLoader());
		System.out.println(vc.getVersion());
		System.err.println("Test error");
		System.out.println("Success");
	}
}
