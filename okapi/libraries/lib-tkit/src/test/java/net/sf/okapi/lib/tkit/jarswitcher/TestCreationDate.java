/*===========================================================================
  Copyright (C) 2008-2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.tkit.jarswitcher;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.util.Date;

import org.junit.Test;

import com.ibm.icu.text.SimpleDateFormat;

public class TestCreationDate {

	@Test
	public void testNioFileAttrs() throws URISyntaxException, IOException {
		Path file = Paths.get(this.getClass().getResource("gold.json").toURI());
		BasicFileAttributes attr = Files.readAttributes(file, BasicFileAttributes.class);

		System.out.println("creationTime: " + attr.creationTime());
		System.out.println("lastAccessTime: " + attr.lastAccessTime());
		System.out.println("lastModifiedTime: " + attr.lastModifiedTime());

		System.out.println("isDirectory: " + attr.isDirectory());
		System.out.println("isOther: " + attr.isOther());
		System.out.println("isRegularFile: " + attr.isRegularFile());
		System.out.println("isSymbolicLink: " + attr.isSymbolicLink());
		System.out.println("size: " + attr.size());
	}
	
	@Test
	public void testCreationDate() throws URISyntaxException, IOException {
		Path file = Paths.get(this.getClass().getResource("gold.json").toURI());
		BasicFileAttributes attr = Files.readAttributes(file, BasicFileAttributes.class);

		FileTime creationTime = attr.creationTime();
		long mils = creationTime.toMillis();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date d = new Date(mils);
		String date = df.format(d);
		System.out.println(date);
	}
	
	@Test
	public void testCreationDate2() {
		String date = VersionInfo.getFileCreationDate(this.getClass().getResource("gold.json"));
		System.out.println(date);
	}
	
}
