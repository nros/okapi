/*===========================================================================
  Copyright (C) 2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.verification;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import net.sf.okapi.common.BOMAwareInputStream;
import net.sf.okapi.common.exceptions.OkapiIOException;

class BlacklistReader {
	
	private BlackTerm nextEntry;
	private BufferedReader reader;
	
	public BlacklistReader() {
		reset();
	}
	
	public void reset() {
		nextEntry = null;
		reader = null;
	}
	
	public void open(File file) {
		try {
			open(new FileInputStream(file));
		}
		catch (Throwable e) {
			throw new OkapiIOException("Error opening the URI.\n" + e.getLocalizedMessage());
		}
	}
	
	public void open(InputStream input) {
		try {
			close();
			
			// Deal with potential BOM
			String encoding = "UTF-8";
			BOMAwareInputStream bis = new BOMAwareInputStream(input, encoding);
			encoding = bis.detectEncoding();
			reader = new BufferedReader(new InputStreamReader(bis, encoding));
			
			// Read input document
			readNext();
		}
		catch (Throwable e) {
			throw new OkapiIOException("Error opening the URI.\n" + e.getLocalizedMessage());
		}
	}
	
	public void close() {
		nextEntry = null;
		try {
			if (reader != null) {
				reader.close();
				reader = null;
			}
		}
		catch (IOException e) {
			throw new OkapiIOException(e);
		}
	}
	
	public boolean hasNext() {
		return (nextEntry != null);
	}
	
	public BlackTerm next() {
		BlackTerm currentEntry = nextEntry;
		readNext();
		return currentEntry;
	}
	
	private void readNext() {
		try {
			nextEntry = null;
			String parts[];
			while (true) {
				String line = reader.readLine();
				if (line == null) {
					return;
				}
				line = line.trim();
				if (line.isEmpty()) {
					continue;
				}
				parts = line.split("\\t");
				if (!parts[0].isEmpty()) {
					break;
				}
			}

			BlackTerm bterm = new BlackTerm();
			bterm.text = parts[0];
			bterm.suggestion = (parts.length == 1) ? "" : parts[1];
			bterm.searchTerm = parts[0].toLowerCase();
			nextEntry = bterm;
		}
		catch (Throwable e) {
			throw new OkapiIOException("Error opening the URI.\n" + e.getLocalizedMessage());
		}
	}
}
