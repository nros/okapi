/*===========================================================================
  Copyright (C) 2009 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.filters.openxml;

import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import net.sf.okapi.common.FileCompare;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class compares two zip files to see if they have
 * the same contents.  The zipsExactlyTheSame method takes
 * two files specified by their file paths and indicates
 * by calling FileCompare whether all files in the zip
 * are exactly the same as each other.  This can be used
 * to compare zip file output with a gold standard zip file.  
 */


public class ZipCompare {

	private final Logger LOGGER = LoggerFactory.getLogger(getClass());
	private FileCompare fc=null;
	public ZipCompare()
	{
		fc = new FileCompare();
	}
	
	public boolean zipsExactlyTheSame (String out, String gold)
		throws IOException
	{
		ZipFile oZipFile = null;
		ZipFile gZipFile = null;
		Set<ZipEntry> outputEntrySet = null;
		Set<ZipEntry> goldEntrySet = null;
		try {
			try
			{
				File oZip = new File(out);
				oZipFile = new ZipFile(oZip);
				List<? extends ZipEntry> l = Collections.list(oZipFile.entries());
				outputEntrySet = new HashSet<ZipEntry>(l);
			}
			catch(Exception e) {
				LOGGER.info("ZipCompare:  Output file {} not found.", out);
				return false;
			}
			try {
				File gZip = new File(gold);
				gZipFile = new ZipFile(gZip);
				List<? extends ZipEntry> l = Collections.list(gZipFile.entries());
				goldEntrySet = new HashSet<ZipEntry>(l);
			}
			catch(Exception e) {
				LOGGER.info("ZipCompare:  Gold file {} not found.", gold);
				return false;
			}
			// We can't directly use equals() on the set of ZipEntry instances,
			// but we can use set operations on the names to detect mismatches.
			Set<String> outputNames = getEntryNames(outputEntrySet);
			Set<String> goldNames = getEntryNames(goldEntrySet);
			
			if (!goldNames.equals(outputNames)) {
				LOGGER.info("ZipCompare: gold parts {} do not match output parts {}", goldNames, outputNames);
				return false;
			}
			
			// Now check the contents of each file.
			for (ZipEntry outputEntry : outputEntrySet) {
				ZipEntry goldEntry = findEntryByName(outputEntry.getName(), goldEntrySet);
				assertNotNull(goldEntry); // this should never fail
				if (!compareContents(gZipFile, goldEntry, oZipFile, outputEntry)) 
					return false;
			}
			
			return true;
		}
		finally {
			if ( oZipFile != null ) oZipFile.close();
			if ( gZipFile != null ) gZipFile.close();
			
		}
	}
	
	private Set<String> getEntryNames(Set<ZipEntry> entries) {
		HashSet<String> s = new HashSet<String>();
		for (ZipEntry e : entries) {
			s.add(e.getName());
		}
		return s;
	}
	
	private ZipEntry findEntryByName(String name, Set<ZipEntry> entries) {
		for (ZipEntry e : entries) {
			if (name.equals(e.getName())) {
				return e;
			}
		}
		return null;
	}
	
	private boolean compareContents(ZipFile gZipFile, ZipEntry gZipEntry, 
								    ZipFile oZipFile, ZipEntry oZipEntry) {
		InputStream ois = null, gis = null;
		try
		{
			ois = oZipFile.getInputStream(oZipEntry);
			gis = gZipFile.getInputStream(gZipEntry);
		}
		catch(Exception e)
		{
			return false;
		}
		
		if (!fc.filesExactlyTheSame(ois,gis)) {
			LOGGER.info("Output and Gold Entry {} differ", oZipEntry.getName()); // DWH 7-16-09
			return false;
		}
		return true;
	}
}
