package net.sf.okapi.tkit.integration;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.FileCompare;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.filters.po.POFilter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RoundTripPoSkeletonBasedTkitsIT
{
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());

	private POFilter poFilter;
	private String[] testFileList;
	private String root;

	@Before
	public void setUp() throws Exception {
		poFilter = new POFilter();
		testFileList = getPoFiles();
		URL url = RoundTripPoSkeletonBasedTkitsIT.class.getResource("/po/Test01.po");
		root = Util.getDirectoryName(url.toURI().getPath()) + File.separator;
	}

	@After
	public void tearDown() throws Exception {
		poFilter.close();
	}

	@Test
	public void roundTripPoFiles() throws FileNotFoundException {
		runTest(false);
		runTest(true);
	}
	
	@SuppressWarnings("resource")
    private void runTest(boolean segment) throws FileNotFoundException {
		for (String f : testFileList) {
			LOGGER.trace(f);
			String xliff = root+f+".xliff";
			String original = root + f;
			String tkitMerged = root+f+".tkitMerged";
			String merged = root+f+".merged";			
			RawDocument rd = new RawDocument(Util.toURI(original), "UTF-8", LocaleId.ENGLISH, LocaleId.SPANISH);
			List<Event> events = FilterTestDriver.getEvents(poFilter, rd, null);	
			if (segment) {
				events = RoundTripUtils.segment(events);
			}
			RoundTripUtils.writeXliff(events, root, xliff);
			String configId = poFilter.getConfigurations().get(0).configId;
	        RoundTripUtils.tkitMerge(original, xliff, tkitMerged, configId);
			RoundTripUtils.legacyMerge(original, xliff, merged, configId);
			FileCompare compare = new FileCompare();
			assertTrue(compare.compareFilesPerLines(tkitMerged, merged, rd.getEncoding()));
			
			RawDocument ord = new RawDocument(Util.toURI(original), "UTF-8", LocaleId.ENGLISH, LocaleId.ENGLISH);
			RawDocument trd = new RawDocument(Util.toURI(tkitMerged), "UTF-8", LocaleId.ENGLISH, LocaleId.ENGLISH);
			List<Event> o = FilterTestDriver.getEvents(poFilter, ord, null);
			List<Event> t = FilterTestDriver.getEvents(poFilter, trd, null);
			assertTrue("Compare original and merged: " + f, FilterTestDriver.compareEvents(o, t, true, false, true, false));
		}
	}

	private static String[] getPoFiles() throws URISyntaxException {
		URL url = RoundTripPoSkeletonBasedTkitsIT.class.getResource("/po/Test01.po");
		File dir = new File(url.toURI()).getParentFile();

		FilenameFilter filter = new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".po");
				//return name.equals("SINGLE_ENTRY_Test_nautilus.af.po");
			}
		};
		return dir.list(filter);
	}
}
