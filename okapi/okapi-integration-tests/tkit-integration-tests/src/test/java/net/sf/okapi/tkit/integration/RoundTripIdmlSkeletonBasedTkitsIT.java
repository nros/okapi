package net.sf.okapi.tkit.integration;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.ZipXMLFileCompare;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.filters.FilterUtil;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.filters.idml.IDMLFilter;
import net.sf.okapi.filters.idml.Parameters;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RoundTripIdmlSkeletonBasedTkitsIT
{
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());

	private IDMLFilter idmlFilter;
	private String[] testFileList;
	private String root;
	private Parameters p;

	@Before
	public void setUp() throws Exception {
		idmlFilter = (IDMLFilter) FilterUtil.createFilter(
				this.getClass().getResource("/idml/okf_idml@ExtractAll.fprm"));
		testFileList = getIdmlFiles();
		URL url = this.getClass().getResource("/idml/idmltest.idml");
		root = Util.getDirectoryName(url.toURI().getPath()) + File.separator;
		p = new Parameters();
		p.load(this.getClass().getResource("/idml/okf_idml@ExtractAll.fprm").toURI(), false);
	}

	@After
	public void tearDown() throws Exception {
		idmlFilter.close();
	}

	@Test
	public void roundTripIdmlFiles() throws FileNotFoundException {
		runTest(false);
		runTest(true);
	}
	
	@SuppressWarnings("resource")
    private void runTest(boolean segment) throws FileNotFoundException {
		for (String f : testFileList) {
			LOGGER.trace(f);
			String xliff = root+f+".xliff";
			String original = root + f;
			String tkitMerged = root+f+".tkitMerged";
			String merged = root+f+".merged";
			RawDocument rd = new RawDocument(Util.toURI(original),
					"UTF-8", LocaleId.ENGLISH, LocaleId.FRENCH);
			List<Event> events = FilterTestDriver.getEvents(idmlFilter, rd, p);
			if (segment) {
				events = RoundTripUtils.segment(events);
			}
			RoundTripUtils.writeXliff(events, root, xliff);
			String configId = "okf_idml@ExtractAll";
	        RoundTripUtils.tkitMerge(original, xliff, tkitMerged, configId, root);
			RoundTripUtils.legacyMerge(original, xliff, merged, configId, root);
			ZipXMLFileCompare compare = new ZipXMLFileCompare(); 
			assertTrue(compare.compareFiles(tkitMerged, merged));
			
			RawDocument ord = new RawDocument(Util.toURI(original), "UTF-8", LocaleId.ENGLISH);
			RawDocument trd = new RawDocument(Util.toURI(tkitMerged), "UTF-8", LocaleId.ENGLISH);
			List<Event> o = FilterTestDriver.getEvents(idmlFilter, ord, p);
			List<Event> t = FilterTestDriver.getEvents(idmlFilter, trd, p);			
			assertTrue("Compare original and merged: " + f, FilterTestDriver.compareEvents(o, t, false, true, false, true));								
		}
	}

	private static String[] getIdmlFiles() throws URISyntaxException {
		// read all files in the test xml directory
		URL url = RoundTripIdmlSkeletonBasedTkitsIT.class.getResource("/idml/idmltest.idml");
		File dir = new File(url.toURI()).getParentFile();

		FilenameFilter filter = new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".idml");
			}
		};
		return dir.list(filter);
	}
}
