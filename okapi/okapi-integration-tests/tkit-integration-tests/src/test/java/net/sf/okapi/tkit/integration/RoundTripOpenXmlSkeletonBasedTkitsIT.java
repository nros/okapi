package net.sf.okapi.tkit.integration;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.ZipFileCompare;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.filters.openxml.OpenXMLFilter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RoundTripOpenXmlSkeletonBasedTkitsIT
{
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());

	private OpenXMLFilter openXmlFilter;
	private String[] testFileList;
	private String root;

	@Before
	public void setUp() throws Exception {
		openXmlFilter = new OpenXMLFilter();
		testFileList = getOpenXmlFiles();
		URL url = RoundTripOpenXmlSkeletonBasedTkitsIT.class.getResource("/openxml/Deli.docx");
		root = Util.getDirectoryName(url.toURI().getPath()) + File.separator;
	}

	@After
	public void tearDown() throws Exception {
		openXmlFilter.close();
	}

	@Test
	public void roundTripOpenXml() throws FileNotFoundException {
		runTest(false);
		runTest(true);
	}
	
	@SuppressWarnings("resource")
    private void runTest(boolean segment) throws FileNotFoundException {
		for (String f : testFileList) {
			LOGGER.trace(f);
			String xliff = root+f+".xliff";
			String original = root + f;
			String tkitMerged = root+f+".tkitMerged";
			String merged = root+f+".merged";
			RawDocument rd = new RawDocument(Util.toURI(original), "UTF-8", LocaleId.ENGLISH);
			List<Event> events = FilterTestDriver.getEvents(openXmlFilter, rd, null);
			if (segment) {
				events = RoundTripUtils.segment(events);
			}
			RoundTripUtils.writeXliff(events, root, xliff);
			String configId = openXmlFilter.getConfigurations().get(0).configId;
	        RoundTripUtils.tkitMerge(original, xliff, tkitMerged, configId);
			RoundTripUtils.legacyMerge(original, xliff, merged, configId);
			ZipFileCompare compare = new ZipFileCompare(); 
			assertTrue(compare.compareFilesPerLines(merged, tkitMerged, rd.getEncoding()));
			
			// FIXME: ruby text causes inline code size differences
			if (!"SampleRuby.docx".equals(f) && !"sample.docx".equals(f)) {
				RawDocument ord = new RawDocument(Util.toURI(original), "UTF-8", LocaleId.ENGLISH);
				RawDocument trd = new RawDocument(Util.toURI(tkitMerged), "UTF-8", LocaleId.ENGLISH);
				List<Event> o = FilterTestDriver.getEvents(openXmlFilter, ord, null);
				List<Event> t = FilterTestDriver.getEvents(openXmlFilter, trd, null);
				assertTrue("Compare original and merged: " + f, FilterTestDriver.compareEvents(o, t, true, true, false, true));	
			}
		}
	}

	private static String[] getOpenXmlFiles() throws URISyntaxException {
		// read all files in the test xml directory
		URL url = RoundTripOpenXmlSkeletonBasedTkitsIT.class.getResource("/openxml/Deli.docx");
		File dir = new File(url.toURI()).getParentFile();

		FilenameFilter filter = new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".docx") || name.endsWith(".xlsx") || name.endsWith(".pptx");
			}
		};
		return dir.list(filter);
	}
}
