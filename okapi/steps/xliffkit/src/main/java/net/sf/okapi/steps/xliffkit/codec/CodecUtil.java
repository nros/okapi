/*===========================================================================
  Copyright (C) 2008-2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.steps.xliffkit.codec;

import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.resource.Code;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.TextContainer;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.common.resource.TextPart;

public class CodecUtil {

	private enum Operation {
		ENCODE,
		DECODE
	}
	
	private static void processTC(TextContainer tc, Operation op, ICodec codec) {
		for (TextPart textPart : tc) {
			if (textPart.isSegment()) {
				
				// Process coded text
				TextFragment tf = textPart.getContent();
				String codedText = tf.getCodedText();
				switch (op) {
				case ENCODE:
					codedText = codec.encode(codedText);					
					break;

				case DECODE:
					codedText = codec.decode(codedText);
					break;
					
				default:
					break;
				}
				tf.setCodedText(codedText);
				
				// Process code data
				for (Code code : tf.getCodes()) {
					if (code.hasOuterData()) {
						code.setOuterData(code.getOuterData());
					}
					
					if (code.hasData()) {
						code.setData(code.getData());
					}
				}
			}
		}
	}
	
	private static void processTU(ITextUnit tu, Operation op, ICodec codec) {
		processTC(tu.getSource(), op, codec);
		for (LocaleId trgLoc : tu.getTargetLocales()) {			
			processTC(tu.getTarget(trgLoc), op, codec);
		}
	}
	
	public static void encodeTextUnit(ITextUnit tu, ICodec codec) {
		processTU(tu, Operation.ENCODE, codec);
	}
	
	public static void decodeTextUnit(ITextUnit tu, ICodec codec) {
		processTU(tu, Operation.DECODE, codec);
	}
}
