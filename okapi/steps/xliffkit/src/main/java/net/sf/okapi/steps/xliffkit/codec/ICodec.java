/*===========================================================================
  Copyright (C) 2008-2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.steps.xliffkit.codec;


public interface ICodec {

	/**
	 * Checks if a given code point can be encoded by this codec. 
	 * @param codePoint the given code point.
	 * @return true if this codec handles the given code point.
	 */
	boolean canEncode(int codePoint);

	/**
	 * Encodes a given text with this codec.
	 * @param text the text to encode.
	 * @return the encoded text.
	 */
	String encode (String text);

	/**
	 * Decodes a given text with this codec.
	 * @param text the text to decode.
	 * @return the decoded text.
	 */
	String decode (String text);

}
