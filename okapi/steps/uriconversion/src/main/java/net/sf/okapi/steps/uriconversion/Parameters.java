/*===========================================================================
  Copyright (C) 2008-2010 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.steps.uriconversion;

import net.sf.okapi.common.StringParameters;

public class Parameters extends StringParameters {
	
	public final static int UNESCAPE = 0;
	
	private static final String CONVERSIONTYPE = "conversionType";
	private static final String UPDATEALL = "updateAll";
	private static final String ESCAPELIST = "escapeList";

	public Parameters () {
		super();
	}
	
	public int getConversionType() {
		return getInteger(CONVERSIONTYPE);
	}

	public void setConversionType(int conversionType) {
		setInteger(CONVERSIONTYPE, conversionType);
	}

	public boolean getUpdateAll() {
		return getBoolean(UPDATEALL);
	}

	public void setUpdateAll(boolean updateAll) {
		setBoolean(UPDATEALL, updateAll);
	}

	public String getEscapeList() {
		return getString(ESCAPELIST);
	}

	public void setEscapeList(String escapeList) {
		setString(ESCAPELIST, escapeList);
	}

	public void reset () {
		super.reset();
		setConversionType(UNESCAPE);
		setUpdateAll(false);
		setEscapeList("%{}[]()&");
	}
}
