/*===========================================================================
  Copyright (C) 2009-2010 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.steps.fullwidthconversion;

import net.sf.okapi.common.StringParameters;

public class Parameters extends StringParameters {
	
	private static final String TOHALFWIDTH = "toHalfWidth";
	private static final String ASCIIONLY = "asciiOnly";
	private static final String INCLUDESLA = "includeSLA";
	private static final String INCLUDELLS = "includeLLS";
	
	public Parameters () {
		super();
	}
	
	public boolean getToHalfWidth() {
		return getBoolean(TOHALFWIDTH);
	}

	public void setToHalfWidth(boolean toHalfWidth) {
		setBoolean(TOHALFWIDTH, toHalfWidth);
	}

	public boolean getAsciiOnly() {
		return getBoolean(ASCIIONLY);
	}

	public void setAsciiOnly(boolean asciiOnly) {
		setBoolean(ASCIIONLY, asciiOnly);
	}

	public boolean getIncludeSLA() {
		return getBoolean(INCLUDESLA);
	}

	public void setIncludeSLA(boolean includeSLA) {
		setBoolean(INCLUDESLA, includeSLA);
	}

	public boolean getIncludeLLS() {
		return getBoolean(INCLUDELLS);
	}

	public void setIncludeLLS(boolean includeLLS) {
		setBoolean(INCLUDELLS, includeLLS);
	}

	public void reset() {
		super.reset();
		setToHalfWidth(true);
		setAsciiOnly(false);
		setIncludeSLA(false);
		setIncludeLLS(false);
	}
}
