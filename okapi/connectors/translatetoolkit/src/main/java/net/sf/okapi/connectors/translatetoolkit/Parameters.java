/*===========================================================================
  Copyright (C) 2009-2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.connectors.translatetoolkit;

import net.sf.okapi.common.ParametersDescription;
import net.sf.okapi.common.StringParameters;
import net.sf.okapi.common.uidescription.CheckboxPart;
import net.sf.okapi.common.uidescription.EditorDescription;
import net.sf.okapi.common.uidescription.IEditorDescriptionProvider;

public class Parameters extends StringParameters implements IEditorDescriptionProvider {

	private static final String HOST = "host"; // Obsolete
	private static final String PORT = "port"; // Obsolete
	private static final String SUPPORTCODES = "supportCodes";
	private static final String URL = "url";
	
	public Parameters () {
		super();
	}
	
	public Parameters (String initialData) {
		super(initialData);
	}
	
	@Override
	public void reset () {
		super.reset();
		setUrl("https://amagama-live.translatehouse.org/api/v1/");
		setSupportCodes(false);
		
		// Obsolete: Use get/setUrl() instead
		setHost("localhost");
		setPort(8080);
	}

	@Deprecated
	public String getHost () {
		return getString(HOST);
	}

	@Deprecated
	public void setHost (String host) {
		setString(HOST, host);
	}

	@Deprecated
	public int getPort () {
		return getInteger(PORT);
	}

	@Deprecated
	public void setPort (int port) {
		setInteger(PORT, port);
	}

	public String getUrl () {
		return getString(URL);
	}

	public void setUrl (String url) {
		setString(URL, url);
	}

	public boolean getSupportCodes () {
		return getBoolean(SUPPORTCODES);
	}

	public void setSupportCodes (boolean supportCodes) {
		setBoolean(SUPPORTCODES, supportCodes);
	}

	@Override
	public ParametersDescription getParametersDescription () {
		ParametersDescription desc = new ParametersDescription(this);
		desc.add(URL, "URL", "The Base part of URL of the TM server");
		// Obsolete desc.add(HOST, "Host", "The host name of the TM server (e.g. localhost)");
		// Obsolete desc.add(PORT, "Port", "The port number of the TM server (e.g. 8080)");
		desc.add(SUPPORTCODES, "Inline codes are letter-coded (e.g. <x1/><g2></g2>)", null);
		return desc;
	}

	@Override
	public EditorDescription createEditorDescription (ParametersDescription paramsDesc) {
		EditorDescription desc = new EditorDescription("Translation Toolkit TM Connector Settings");
		desc.addTextInputPart(paramsDesc.get(URL));
		// Obsolete desc.addTextInputPart(paramsDesc.get(HOST));
		// Obsolete desc.addTextInputPart(paramsDesc.get(PORT));
		CheckboxPart cbp = desc.addCheckboxPart(paramsDesc.get(SUPPORTCODES));
		cbp.setVertical(true);
		return desc;
	}

}
