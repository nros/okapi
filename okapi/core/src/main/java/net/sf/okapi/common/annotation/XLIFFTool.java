/*===========================================================================
  Copyright (C) 2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.common.annotation;

import net.sf.okapi.common.Util;

/**
 * Annotation representing the <tool> element within an XLIFF
 * <file><header>. The set of tool elements should be contained within
 * the XLIFFToolAnnotation on a StartSubDocument Event.
 */
public class XLIFFTool {
	// Required
	private String id, name;
	// Optional
	private String version, company;
	// Content
	private StringBuilder skel = new StringBuilder();

	public XLIFFTool(String toolId, String toolName) {
		this.id = toolId;
		this.name = toolName;
	}

	public String getId() {
		return this.id;
	}

	public String getName() {
		return this.name;
	}

	public void setVersion(String toolVersion) {
		this.version = toolVersion;
	}

	public void setCompany(String toolCompany) {
		this.company = toolCompany;
	}

	public void addSkeletonContent(String text) {
		skel.append(text);
	}

	public String getSkel() {
		return skel.toString();
	}

	public String getVersion() {
		return version;
	}

	public String getCompany() {
		return company;
	}
	
	public String toXML() {
		StringBuilder sb = new StringBuilder();
		sb.append("<tool");
		sb.append(" tool-id=\"")
		  .append(Util.escapeToXML(id, 1, false, null)).append("\"");
		sb.append(" tool-name=\"")
		  .append(Util.escapeToXML(name, 1, false, null)).append("\"");
		if (version != null) {
			sb.append(" tool-version=\"")
			  .append(Util.escapeToXML(version, 1, false, null)).append("\"");
		}
		if (company != null) {
			sb.append(" tool-company=\"")
			  .append(Util.escapeToXML(company, 1, false, null)).append("\"");
		}
		sb.append(">");
		sb.append(skel);
		sb.append("</tool>");
		return sb.toString();
	}
	
}
