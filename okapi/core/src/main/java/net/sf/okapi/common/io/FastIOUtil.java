/*===========================================================================
  Copyright (C) 2010-2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.common.io;

public class FastIOUtil {
	
	/*
	 * Ensures that a range given by an offset and a length fits an array of
	 * given length.
	 * 
	 * <P>This method may be used whenever an array range check is needed. *
	 */
	public static  void ensureOffsetLength(final int arrayLength, final int offset,
			final int length) {
		if (offset < 0)
			throw new ArrayIndexOutOfBoundsException("Offset (" + offset
					+ ") is negative");
		if (length < 0)
			throw new IllegalArgumentException("Length (" + length
					+ ") is negative");
		if (offset + length > arrayLength)
			throw new ArrayIndexOutOfBoundsException("Last index ("
					+ (offset + length) + ") is greater than array length ("
					+ arrayLength + ")");
	}
}
