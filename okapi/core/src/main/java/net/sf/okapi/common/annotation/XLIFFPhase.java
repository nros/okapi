/*===========================================================================
  Copyright (C) 2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.common.annotation;

import net.sf.okapi.common.Util;

/**
 * Annotation representing the <phase> element within an XLIFF <file><header>.
 * The set of phase elements should be contained within the XLIFFPhaseAnnotation
 * on a StartSubDocument Event.
 */
public class XLIFFPhase {
	// Required
	private String phase_name, process_name;
	// TODO: Optional attribute handling
//	private String company_name, date, job_id, contact_name, contact_email, contact_phone;
//	private XLIFFTool tool;
	// TODO: Handle <note> elements as well.
	private StringBuilder skel = new StringBuilder();

	public XLIFFPhase(String phaseName, String processName) {
		this.phase_name = phaseName;
		this.process_name = processName;
	}

	public String getPhaseName() {
		return this.phase_name;
	}

	public String getProcessName() {
		return this.process_name;
	}

	public void addSkeletonContent(String text) {
		skel.append(text);
	}

	public String toXML() {
		StringBuilder sb = new StringBuilder();
		sb.append("<phase");
		sb.append(" phase-name=\"")
		  .append(Util.escapeToXML(phase_name, 1, false, null)).append("\"");
		sb.append(" process-name=\"")
		  .append(Util.escapeToXML(process_name, 1, false, null)).append("\"");
		sb.append(">");
		sb.append(skel);
		sb.append("</phase>");
		return sb.toString();
	}
}
