/*===========================================================================
  Copyright (C) 2010-2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.common;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.util.zip.CRC32;
import java.util.zip.CheckedInputStream;
import java.util.zip.GZIPInputStream;

import net.sf.okapi.common.exceptions.OkapiFileNotFoundException;
import net.sf.okapi.common.exceptions.OkapiIOException;
import net.sf.okapi.common.io.FileCachedInputStream;

public class StreamUtil {

	/**
	 * Reads an InputStream into an array of bytes.
	 * @param in the input stream to read.
	 * @return the array of bytes read.
	 * @throws IOException if an error occurs.
	 */
	public static byte[] inputStreamToBytes(InputStream in) throws IOException
	{
		ByteArrayOutputStream out = null;
		try {
			out = new ByteArrayOutputStream(1024);
			byte[] buffer = new byte[1024];
			int len;

			while((len = in.read(buffer)) >= 0) {
				out.write(buffer, 0, len);
			}
		}
		finally {
			if (in != null) {
				in.close();
			}
			if (out != null) {
				out.close();
			}
		}

		return out.toByteArray();
	}
	
	public static void copy(File in, OutputStream out) {
		FileInputStream inputStream = null;
		try {
			inputStream = new FileInputStream(in);
			copy(inputStream, out);
		} catch (FileNotFoundException e) {
			throw new OkapiFileNotFoundException(e);
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					throw new OkapiIOException(e);
				}
			}
		}
	}

	public static String streamAsString(InputStream in, String encoding) {
		BufferedReader reader = null;
		StringBuilder tmp = null;
		try {
			reader = new BufferedReader(new InputStreamReader(in, encoding));
			tmp = new StringBuilder();
			char[] buf = new char[2048];
			int count = 0;
			while (( count = reader.read(buf)) != -1 ) {
				tmp.append(buf, 0, count);
			}		
		} catch (IOException e) {
			throw new OkapiIOException(e);
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					throw new OkapiIOException(e);
				}
			}
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					throw new OkapiIOException(e);
				}
			}
		}
		
        return Util.normalizeNewlines(tmp.toString());
    }
	
	public static String streamUtf8AsString(InputStream in) {
        return streamAsString(in, "UTF-8");
    }
	
	public static InputStream stringAsStream(String str, String encoding) {
		InputStream is = null;
		try {
			is = new ByteArrayInputStream(str.getBytes(encoding));
		} catch (IOException e) {
			throw new OkapiIOException(e);
		}
		return is;
	}
	
	public static long calcCRC(InputStream in) {
		CheckedInputStream cis = new CheckedInputStream(in, new CRC32());
		byte[] buf = new byte[1024];
        try {
			while(cis.read(buf) >= 0) {
			}
		} catch (IOException e) {
			throw new OkapiIOException(e);
		}
        return cis.getChecksum().getValue();
	}

	/**
	 * Copies a {@link ReadableByteChannel} to a {@link WritableByteChannel}.
	 * @param inChannel the input Channel.
	 * @param outChannel the output Channel.
	 * @throws OkapiIOException if an error occurs.
	 */
	public static void copy(ReadableByteChannel inChannel, WritableByteChannel outChannel)
	{
		 copy(inChannel, outChannel, true);
	}
	
	/**
	 * Copies a {@link ReadableByteChannel} to a {@link WritableByteChannel}.
	 * @param inChannel the input Channel.
	 * @param outChannel the output Channel.
	 * @param closeChannels close in and out channels? 
	 * @throws OkapiIOException if an error occurs.
	 */
	public static void copy(ReadableByteChannel inChannel, WritableByteChannel outChannel, boolean closeChannels)
	{
		// allocateDirect may be faster, but the risk of 
		// out of memory and memory leak is higher
		// see http://www.informit.com/articles/article.aspx?p=2133373&seqNum=12
		ByteBuffer buffer = ByteBuffer.allocate(16 * 1024);
		try {
			while (inChannel.read(buffer) != -1) {
				// prepare the buffer to be drained
				buffer.flip();
				// write to the channel, may block
				outChannel.write(buffer);
				// If partial transfer, shift remainder down
				// If buffer is empty, same as doing clear()
				buffer.compact();
			}
	
			// EOF will leave buffer in fill state
			buffer.flip();
			// make sure the buffer is fully drained.
			while (buffer.hasRemaining()) {
				outChannel.write(buffer);
			}
		}
		catch ( IOException e ) {
			throw new OkapiIOException(e);
		} finally {
			buffer.clear();
			buffer = null;
			try {
				if (inChannel != null && closeChannels) inChannel.close();
			} catch (IOException e) {
				throw new OkapiIOException(e);
			}
			try {
				// don't close FileCachedInputStream as this
				// resets the position and must call reOpen to use it
				if (outChannel != null 
						&& !(outChannel instanceof FileCachedInputStream)
						&& closeChannels) {
					outChannel.close();
				}
			} catch (IOException e) {			
				throw new OkapiIOException(e);
			}
		}
	}

	
	/**
	 * Copies an {@link InputStream} to a File.
	 * @param is the input stream.
	 * @param outputFile the output {@link File}.
	 * @throws OkapiIOException if an error occurs.
	 */
	public static void copy(InputStream in, File outputFile) {
		FileOutputStream outputStream = null;
		try {
			if (!outputFile.exists()) {
				Util.createDirectories(outputFile.getAbsolutePath());
				outputFile.createNewFile();
			}
			outputStream = new FileOutputStream(outputFile);
			copy(in, outputStream);
		} catch (IOException e) {
			throw new OkapiIOException(e);
		} finally {
			if (outputStream != null) {
				try {
					outputStream.close();
				} catch (IOException e) {
					throw new OkapiIOException(e);
				} 			
			}
		}
	}

	/**
	 * Copies an {@link InputStream} to a File.
	 * @param is the input stream.
	 * @param outputPath the output path.
	 * @throws OkapiIOException if an error occurs.
	 */
	@SuppressWarnings("resource")
	public static void copy(InputStream is, String outputPath)
	{
		ReadableByteChannel inChannel = null;
		WritableByteChannel outChannel = null;
		FileOutputStream outputStream = null;
		try {
			outputStream = new FileOutputStream(new File(outputPath));
			inChannel = Channels.newChannel(is);
			outChannel = outputStream.getChannel();
			copy(inChannel, outChannel);
		}
		catch ( FileNotFoundException e ) {
			throw new OkapiIOException(e);
		}
		finally {
			try {
				if (is != null) is.close();
				if (outputStream != null) outputStream.close();
			}
			catch ( IOException e ) {
				throw new OkapiIOException(e);
			}
		}
	}
	
	/**
	 * Copies an {@link InputStream} to an {@link OutputStream}.
	 * @param is the input stream.
	 * @param outputStream the output stream.
	 * @throws OkapiIOException if an error occurs.
	 */
	@SuppressWarnings("resource")
	public static void copy(InputStream is, OutputStream outputStream)
	{
		ReadableByteChannel inChannel = null;
		WritableByteChannel outChannel = null;
		try {
			inChannel = Channels.newChannel(is);
			outChannel = Channels.newChannel(outputStream);
			// channels are closed here
			copy(inChannel, outChannel);
		}
		finally {
			try {
				if (is != null) is.close();
				if (outputStream != null) outputStream.close();
			}
			catch ( IOException e ) {
				throw new OkapiIOException(e);
			}
		}
	}

	/**
	 * Copies one file to another.
	 * @param in the input file.
	 * @param out the output file.
	 * @throws OkapiIOException if an error occurs.
	 */
	@SuppressWarnings("resource")
	public static void copy(File in, File out)
	{
		FileChannel inChannel = null;
		FileChannel outChannel = null;
		FileInputStream inputStream = null;
		FileOutputStream outputStream = null;
		try {
			inputStream = new FileInputStream(in);
			outputStream = new FileOutputStream(out);
			
			// these are closed in copy(inChannel, outChannel);
			inChannel = inputStream.getChannel();
			outChannel = outputStream.getChannel();
			copy(inChannel, outChannel);
		}
		catch ( IOException e ) {
			throw new OkapiIOException(e);
		}
		finally {
			try {
				if ( inputStream != null ) inputStream.close();
				if ( outputStream != null ) outputStream.close();				
			}
			catch ( IOException e ) {
				throw new OkapiIOException(e);
			}
		}
	}

	/**
	 * Copies a file from one location to another.
	 * @param fromPath the path of the file to copy.
	 * @param toPath the path of the copy to make.
	 * @param move true to move the file, false to copy it.
	 * @throws OkapiIOException if an error occurs.
	 */
	public static void copy(String fromPath, String toPath, boolean move)
	{
		FileChannel ic = null;
		FileChannel oc = null;
		FileInputStream inputStream = null;
		FileOutputStream outputStream = null;
		try {
			Util.createDirectories(toPath);
			inputStream = new FileInputStream(fromPath);
			outputStream = new FileOutputStream(toPath); 
			ic = inputStream.getChannel();
			oc = outputStream.getChannel();
			copy(ic, oc);
			if ( move ) {
				ic.close();
				ic = null;
				File file = new File(fromPath);
				file.delete();
			}
		}
		catch ( IOException e ) {			
			throw new OkapiIOException(e);
		}
		finally {
			try {
				if ( inputStream != null ) inputStream.close();
				if ( outputStream != null ) outputStream.close();
			}
			catch ( IOException e ) {
				throw new OkapiIOException(e);
			}
		}
	}
	
	public static FileCachedInputStream createResettableStream(InputStream is, int bufferSize) throws IOException {
		FileCachedInputStream ifcis;
		try {
			ifcis = new FileCachedInputStream(bufferSize);
			copy(Channels.newChannel(is), ifcis);
		} catch (IOException e) {
			is.close();
			throw new OkapiIOException("Error copying inputstream to FileCachedInputStream", e);
		} finally {
			is.close();
		}
		
		// create a mark on the first byte
		// this is expected by some callers
		// that reset the stream themselves
		ifcis.mark(Integer.MAX_VALUE);
		return ifcis;
	}
	
	/*
	 * Determines if a byte array is compressed. The java.util.zip GZip
	 * Implementation does not expose the GZip header so it is difficult to determine
	 * if a string is compressed.
	 * 
	 * @param bytes an array of bytes
	 * @return true if the array is compressed or false otherwise
	 * @throws java.io.IOException if the byte array couldn't be read
	 */
	 public boolean isCompressed(byte[] bytes) throws IOException
	 {
	      if ((bytes == null) || (bytes.length < 2))
	      {
	           return false;
	      }
	      else
	      {
	    	  return ((bytes[0] == (byte) (GZIPInputStream.GZIP_MAGIC)) && (bytes[1] == (byte) (GZIPInputStream.GZIP_MAGIC >> 8)));
	      }
	 }
}
